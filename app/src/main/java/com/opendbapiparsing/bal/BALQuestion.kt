package com.opendbapiparsing.bal

import android.content.ContentValues
import android.content.Context
import com.opendbapiparsing.dal.DALQuestion
import com.opendbapiparsing.model.QuestionDataModel
import java.util.*

class BALQuestion(context: Context?) : DALQuestion(context) {

    fun getAllQuestions(): MutableList<QuestionDataModel> {
        val list: MutableList<QuestionDataModel> = ArrayList()
        val cursor = selectAllQuestions()

        do {
            val questionDataModel = QuestionDataModel()

            questionDataModel.id = cursor.getInt(cursor.getColumnIndex(ID))
            questionDataModel.category = cursor.getString(cursor.getColumnIndex(CATEGORY))
            questionDataModel.difficulty = cursor.getString(cursor.getColumnIndex(DIFFICULTY))
            questionDataModel.type = cursor.getString(cursor.getColumnIndex(TYPE))
            questionDataModel.question = cursor.getString(cursor.getColumnIndex(QUESTION))
            questionDataModel.correctAnswer = cursor.getString(cursor.getColumnIndex(ANSWER))

            val options: MutableList<String> = ArrayList()
            options.add(cursor.getString(cursor.getColumnIndex(OPTION1)))
            options.add(cursor.getString(cursor.getColumnIndex(OPTION2)))
            options.add(cursor.getString(cursor.getColumnIndex(OPTION3)))
            options.add(cursor.getString(cursor.getColumnIndex(OPTION4)))
            questionDataModel.incorrectAnswers = options

            list.add(questionDataModel)
        } while (cursor.moveToNext())

        return list
    }

    fun addQuestion(cv: ContentValues?): Long {
        return insertQuestion(cv)
    }

    fun deleteAllQuestions() {
        deleteQuestions()
    }
}