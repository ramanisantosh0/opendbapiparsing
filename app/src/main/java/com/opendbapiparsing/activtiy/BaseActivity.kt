package com.opendbapiparsing.activtiy

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.opendbapiparsing.R

open class BaseActivity : AppCompatActivity() {
    var queue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        queue = Volley.newRequestQueue(this)
    }

    fun callApi(url: String?, onApiResponse: OnApiResponse) {
        showProgressDialog()
        val stringRequest = StringRequest(Request.Method.GET, url,
                { response -> onApiResponse.onSuccess(response) }) { }
        queue!!.add(stringRequest)
    }

    interface OnApiResponse {
        fun onSuccess(response: String)
    }

    private fun showProgressDialog() {
        if (progressDialog == null) {
            val view = layoutInflater.inflate(R.layout.dialog_progress, null)
            val builder = AlertDialog.Builder(this)
            builder.setView(view)
            builder.setCancelable(false)
            progressDialog = builder.create()
        }
        progressDialog!!.show()
    }

    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) progressDialog!!.dismiss()
    }

    companion object {
        private var progressDialog: AlertDialog? = null
    }
}