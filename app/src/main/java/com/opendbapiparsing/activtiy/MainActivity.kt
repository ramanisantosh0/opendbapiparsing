package com.opendbapiparsing.activtiy

import android.annotation.SuppressLint
import android.content.ContentValues
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.Toast
import com.opendbapiparsing.R
import com.opendbapiparsing.adapter.ActvCategoryAdapter
import com.opendbapiparsing.bal.BALQuestion
import com.opendbapiparsing.dal.DALQuestion
import com.opendbapiparsing.model.CategoryModel
import com.opendbapiparsing.model.QuestionDataModel
import com.opendbapiparsing.model.QuestionModel
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class MainActivity : BaseActivity() {
    var url = "https://opentdb.com/api.php?"
    var amount = "amount="
    var category = ""
    var difficulty = ""
    var type = ""

    var categoryList: MutableList<CategoryModel> = ArrayList()
    var actvCategoryAdapter: ActvCategoryAdapter? = null
    var difficultyList: MutableList<String> = ArrayList()
    var typeList: MutableList<String> = ArrayList()
    var questionsList: MutableList<QuestionDataModel> = ArrayList()
    var dbQuestionsList: MutableList<QuestionDataModel> = ArrayList()
    var queIndex = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        BALQuestion(this).deleteAllQuestions()

        btnGetQuestion?.setOnClickListener {
            if (TextUtils.isEmpty(etNoOfQuestion?.text.toString().trim()) || !TextUtils.isDigitsOnly(etNoOfQuestion?.text.toString().trim())) {
                tilNoOfQuestion?.error = "Please enter Question Count"
                tilNoOfQuestion?.isErrorEnabled = true
            } else {
                tilNoOfQuestion?.isErrorEnabled = false
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etNoOfQuestion?.windowToken, 0)
                callGetQuestionApi()
            }
        }

        btnPrev.setOnClickListener {
            if (queIndex > 0) {
                queIndex--
                setQuestions()
            }
        }

        btnNext.setOnClickListener {
            if (queIndex < dbQuestionsList.size - 1) {
                queIndex++
                setQuestions()
            }
        }

        difficultyList.clear()
        difficultyList.add("Any Difficulty")
        difficultyList.add("Easy")
        difficultyList.add("Medium")
        difficultyList.add("Hard")
        typeList.clear()
        typeList.add("Any Type")
        typeList.add("Multiple Choice")
        typeList.add("True / False")

        callApi("https://opentdb.com/api_category.php", object : OnApiResponse {
            override fun onSuccess(response: String) {
                parseCategoryJson(response)
            }
        })
    }

    private fun callGetQuestionApi() {
        val furl = url + amount + etNoOfQuestion!!.text.toString().trim() + category.toLowerCase() + difficulty.toLowerCase() + type.toLowerCase()
        callApi(furl, object : OnApiResponse {
            override fun onSuccess(response: String) {
                val questionModel = parseQuestionJson(response)
                if (questionModel.responseCode == 0) {
                    questionsList.clear()
                    questionsList.addAll(questionModel.results)
                    insertIntoDatabase()
                } else {
                    dismissProgressDialog()
                    Toast.makeText(this@MainActivity, "No Question Found", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun insertIntoDatabase() {
        val balQuestion = BALQuestion(this)
        for (i in questionsList.indices) {
            val cv = ContentValues()
            cv.put(DALQuestion.CATEGORY, questionsList[i].category)
            cv.put(DALQuestion.DIFFICULTY, questionsList[i].difficulty)
            cv.put(DALQuestion.TYPE, questionsList[i].type)
            cv.put(DALQuestion.QUESTION, questionsList[i].question)
            cv.put(DALQuestion.ANSWER, questionsList[i].correctAnswer)
            if (questionsList[i].type.equals("multiple", ignoreCase = true)) {
                val ans = Random().nextInt(4)
                when (ans) {
                    0 -> {
                        cv.put(DALQuestion.OPTION1, questionsList[i].correctAnswer)
                        cv.put(DALQuestion.OPTION2, questionsList[i].incorrectAnswers[0])
                        if (questionsList[i].incorrectAnswers.size > 1) {
                            cv.put(DALQuestion.OPTION3, questionsList[i].incorrectAnswers[1])
                            cv.put(DALQuestion.OPTION4, questionsList[i].incorrectAnswers[2])
                        } else {
                            cv.put(DALQuestion.OPTION3, "")
                            cv.put(DALQuestion.OPTION4, "")
                        }
                    }
                    1 -> {
                        cv.put(DALQuestion.OPTION1, questionsList[i].incorrectAnswers[0])
                        cv.put(DALQuestion.OPTION2, questionsList[i].correctAnswer)
                        if (questionsList[i].incorrectAnswers.size > 1) {
                            cv.put(DALQuestion.OPTION3, questionsList[i].incorrectAnswers[1])
                            cv.put(DALQuestion.OPTION4, questionsList[i].incorrectAnswers[2])
                        } else {
                            cv.put(DALQuestion.OPTION3, "")
                            cv.put(DALQuestion.OPTION4, "")
                        }
                    }
                    2 -> {
                        cv.put(DALQuestion.OPTION1, questionsList[i].incorrectAnswers[0])
                        cv.put(DALQuestion.OPTION3, questionsList[i].correctAnswer)
                        if (questionsList[i].incorrectAnswers.size > 1) {
                            cv.put(DALQuestion.OPTION2, questionsList[i].incorrectAnswers[1])
                            cv.put(DALQuestion.OPTION4, questionsList[i].incorrectAnswers[2])
                        } else {
                            cv.put(DALQuestion.OPTION2, "")
                            cv.put(DALQuestion.OPTION4, "")
                        }
                    }
                    3 -> {
                        cv.put(DALQuestion.OPTION1, questionsList[i].incorrectAnswers[0])
                        cv.put(DALQuestion.OPTION4, questionsList[i].correctAnswer)
                        if (questionsList[i].incorrectAnswers.size > 1) {
                            cv.put(DALQuestion.OPTION2, questionsList[i].incorrectAnswers[1])
                            cv.put(DALQuestion.OPTION3, questionsList[i].incorrectAnswers[2])
                        } else {
                            cv.put(DALQuestion.OPTION2, "")
                            cv.put(DALQuestion.OPTION3, "")
                        }
                    }
                }
            } else {
                cv.put(DALQuestion.OPTION1, questionsList[i].correctAnswer)
                cv.put(DALQuestion.OPTION2, questionsList[i].incorrectAnswers[0])
                cv.put(DALQuestion.OPTION3, "")
                cv.put(DALQuestion.OPTION4, "")
            }
            balQuestion.addQuestion(cv)
        }
        dismissProgressDialog()
        dbQuestionsList.clear()
        dbQuestionsList = balQuestion.getAllQuestions()
        setQuestions()
        llQuestions!!.visibility = View.VISIBLE
        llFilter!!.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    private fun setQuestions() {
        if (dbQuestionsList.isNotEmpty()) {
            setNextPrevButtons()
            tvQueNo!!.text = "Q. " + (queIndex + 1).toString() + " of " + dbQuestionsList.size
            tvQuestion!!.text = "" + dbQuestionsList[queIndex].question
            tvOption1!!.text = "" + dbQuestionsList[queIndex].incorrectAnswers[0]
            tvOption2!!.text = "" + dbQuestionsList[queIndex].incorrectAnswers[1]
            tvOption3!!.text = "" + dbQuestionsList[queIndex].incorrectAnswers[2]
            tvOption4!!.text = "" + dbQuestionsList[queIndex].incorrectAnswers[3]
            if (dbQuestionsList[queIndex].type.equals("multiple", ignoreCase = true)) {
                tvOption3!!.visibility = View.VISIBLE
                tvOption4!!.visibility = View.VISIBLE
            } else {
                tvOption3!!.visibility = View.GONE
                tvOption4!!.visibility = View.GONE
            }

            setTrueOption(dbQuestionsList[queIndex].correctAnswer)
        }
    }

    private fun setTrueOption(correct: String) {
        when {
            correct.equals(dbQuestionsList[queIndex].incorrectAnswers[0], ignoreCase = true) -> {
                tvOption1.background = resources.getDrawable(R.drawable.true_option_background)
                tvOption2.background = resources.getDrawable(R.drawable.option_background)
                tvOption3.background = resources.getDrawable(R.drawable.option_background)
                tvOption4.background = resources.getDrawable(R.drawable.option_background)
            }
            correct.equals(dbQuestionsList[queIndex].incorrectAnswers[1], ignoreCase = true) -> {
                tvOption1.background = resources.getDrawable(R.drawable.option_background)
                tvOption2.background = resources.getDrawable(R.drawable.true_option_background)
                tvOption3.background = resources.getDrawable(R.drawable.option_background)
                tvOption4.background = resources.getDrawable(R.drawable.option_background)
            }
            correct.equals(dbQuestionsList[queIndex].incorrectAnswers[2], ignoreCase = true) -> {
                tvOption1.background = resources.getDrawable(R.drawable.option_background)
                tvOption2.background = resources.getDrawable(R.drawable.option_background)
                tvOption3.background = resources.getDrawable(R.drawable.true_option_background)
                tvOption4.background = resources.getDrawable(R.drawable.option_background)
            }
            correct.equals(dbQuestionsList[queIndex].incorrectAnswers[3], ignoreCase = true) -> {
                tvOption1.background = resources.getDrawable(R.drawable.option_background)
                tvOption2.background = resources.getDrawable(R.drawable.option_background)
                tvOption3.background = resources.getDrawable(R.drawable.option_background)
                tvOption4.background = resources.getDrawable(R.drawable.true_option_background)
            }
        }
    }

    private fun setNextPrevButtons() {
        if (queIndex == 0) {
            btnPrev!!.visibility = View.GONE
        } else {
            btnPrev!!.visibility = View.VISIBLE
        }
        if (queIndex == dbQuestionsList.size - 1) {
            btnNext!!.visibility = View.GONE
        } else {
            btnNext!!.visibility = View.VISIBLE
        }
    }

    private fun parseQuestionJson(response: String): QuestionModel {
        val questionModel = QuestionModel()
        try {
            val jsonResponse = JSONObject(response)
            if (jsonResponse.has("response_code") && !jsonResponse.isNull("response_code")) {
                questionModel.responseCode = jsonResponse.getInt("response_code")
            }
            if (jsonResponse.has("results") && !jsonResponse.isNull("results") && jsonResponse.opt("results") is JSONArray) {
                val resultsArray = jsonResponse.getJSONArray("results")
                val list: MutableList<QuestionDataModel> = ArrayList()
                for (i in 0 until resultsArray.length()) {
                    val itemObject = resultsArray.getJSONObject(i)
                    val questionDataModel = QuestionDataModel()
                    if (itemObject.has("category") && !itemObject.isNull("category")) {
                        questionDataModel.category = itemObject.getString("category")
                    } else {
                        questionDataModel.category = ""
                    }
                    if (itemObject.has("type") && !itemObject.isNull("type")) {
                        questionDataModel.type = itemObject.getString("type")
                    } else {
                        questionDataModel.type = ""
                    }
                    if (itemObject.has("difficulty") && !itemObject.isNull("difficulty")) {
                        questionDataModel.difficulty = itemObject.getString("difficulty")
                    } else {
                        questionDataModel.difficulty = ""
                    }
                    if (itemObject.has("question") && !itemObject.isNull("question")) {
                        questionDataModel.question = itemObject.getString("question")
                    } else {
                        questionDataModel.question = ""
                    }
                    if (itemObject.has("correct_answer") && !itemObject.isNull("correct_answer")) {
                        questionDataModel.correctAnswer = itemObject.getString("correct_answer")
                    } else {
                        questionDataModel.correctAnswer = ""
                    }
                    if (itemObject.has("incorrect_answers") && !itemObject.isNull("incorrect_answers")) {
                        val incorrectAnswers = itemObject.getJSONArray("incorrect_answers")
                        val incorrectAnswersList: MutableList<String> = ArrayList()
                        for (j in 0 until incorrectAnswers.length()) {
                            incorrectAnswersList.add(incorrectAnswers.getString(j))
                        }
                        questionDataModel.incorrectAnswers = incorrectAnswersList
                    } else {
                        questionDataModel.incorrectAnswers = ArrayList()
                    }
                    list.add(questionDataModel)
                }
                questionModel.results = list
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return questionModel
    }

    private fun parseCategoryJson(response: String) {
        categoryList.clear()
        try {
            val jsonResponse = JSONObject(response)
            if (jsonResponse.has("trivia_categories") && jsonResponse.opt("trivia_categories") is JSONArray) {
                val list: MutableList<CategoryModel> = ArrayList()
                list.add(CategoryModel(-1, "Any Category"))
                val triviaCategoryArray = jsonResponse.getJSONArray("trivia_categories")
                for (i in 0 until triviaCategoryArray.length()) {
                    val itemObject = triviaCategoryArray.getJSONObject(i)
                    if (itemObject.has("id") && !itemObject.isNull("id") && itemObject.has("name") && !itemObject.isNull("name")) {
                        list.add(CategoryModel(itemObject.getInt("id"), itemObject.getString("name")))
                    }
                }
                categoryList.addAll(list)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        setCategoryActvAdapter()
    }

    private fun setCategoryActvAdapter() {
        if (categoryList.isNotEmpty()) {
            if (actvCategoryAdapter == null) {
                actvCategoryAdapter = ActvCategoryAdapter(this, android.R.layout.simple_list_item_1, categoryList)
                actvCategory!!.setAdapter(actvCategoryAdapter)
                actvCategory!!.threshold = 1
                actvCategory!!.onItemClickListener = OnItemClickListener { parent, view, position, id -> category = if ((parent.getItemAtPosition(position) as CategoryModel).categoryID != -1) "&category=" + (parent.getItemAtPosition(position) as CategoryModel).categoryID else "" }
            } else {
                actvCategoryAdapter!!.notifyDataSetChanged()
            }
        }
        actvDifficulty!!.setAdapter(ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, difficultyList))
        actvDifficulty!!.onItemClickListener = OnItemClickListener { parent, _, position, _ -> difficulty = if (parent.getItemAtPosition(position) != "Any Difficulty") "&difficulty=" + parent.getItemAtPosition(position) else "" }
        actvType!!.setAdapter(ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, typeList))
        actvType!!.onItemClickListener = OnItemClickListener { parent, _, position, _ -> type = if (parent.getItemAtPosition(position) != "Any Type") "&type=" + parent.getItemAtPosition(position) else "" }
        dismissProgressDialog()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionFilter) {
            llFilter!!.visibility = View.VISIBLE
            llQuestions!!.visibility = View.GONE
            queIndex = 0
            BALQuestion(this).deleteAllQuestions()
        }
        return super.onOptionsItemSelected(item)
    }
}