package com.opendbapiparsing.adapter

import android.R
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.opendbapiparsing.model.CategoryModel
import java.util.*

class ActvCategoryAdapter(context: Context, resource: Int, objects: List<CategoryModel>) : ArrayAdapter<CategoryModel?>(context, resource, objects) {
    private val resourceId: Int
    private var items: List<CategoryModel> = ArrayList()
    private var tempItems: List<CategoryModel> = ArrayList()
    private var suggestions: MutableList<CategoryModel> = ArrayList()
    private val categoryFilter: Filter = object : Filter() {
        override fun convertResultToString(resultValue: Any): CharSequence {
            val category = resultValue as CategoryModel
            return category.categoryName
        }

        override fun performFiltering(charSequence: CharSequence): FilterResults {
            return if (charSequence != null) {
                suggestions.clear()
                for (category in tempItems) {
                    if (category.categoryName.toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(category)
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions.size
                filterResults
            } else {
                FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence, filterResults: FilterResults) {
            val tempValues = filterResults.values as ArrayList<CategoryModel>
            if (filterResults.count > 0) {
                clear()
                for (categoryObj in tempValues) {
                    add(categoryObj)
                }
                notifyDataSetChanged()
            } else {
                clear()
                notifyDataSetChanged()
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        try {
            if (convertView == null) {
                val inflater = (context as Activity).layoutInflater
                view = inflater.inflate(resourceId, parent, false)
            }
            val item = getItem(position)
            val name = view!!.findViewById<TextView>(R.id.text1)
            name.text = item!!.categoryName
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view!!
    }

    override fun getItemId(position: Int): Long {
        return items[position].categoryID.toLong()
    }

    override fun getItem(position: Int): CategoryModel? {
        return items[position]
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getFilter(): Filter {
        return categoryFilter
    }

    init {
        items = objects
        resourceId = resource
        tempItems = ArrayList(items)
        suggestions = ArrayList()
    }
}