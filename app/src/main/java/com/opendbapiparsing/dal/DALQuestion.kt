package com.opendbapiparsing.dal

import android.content.ContentValues
import android.content.Context
import android.database.Cursor

open class DALQuestion(context: Context?) : DBHelper(context) {

    protected fun selectAllQuestions(): Cursor {
        val query = "SELECT $ID,$CATEGORY,$TYPE,$DIFFICULTY,$QUESTION,$ANSWER,$OPTION1,$OPTION2,$OPTION3,$OPTION4 FROM $TBL_QUESTIONS"
        val db = readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToFirst()
        return cursor
    }

    protected fun insertQuestion(cv: ContentValues?): Long {
        val db = writableDatabase
        return db.insert(TBL_QUESTIONS, null, cv)
    }

    protected fun deleteQuestions() {
        val db = writableDatabase
        db.delete(TBL_QUESTIONS, null, null)
    }

    companion object {
        const val ID = "id"
        const val CATEGORY = "category"
        const val TYPE = "type"
        const val DIFFICULTY = "difficulty"
        const val QUESTION = "question"
        const val ANSWER = "answer"
        const val OPTION1 = "option1"
        const val OPTION2 = "option2"
        const val OPTION3 = "option3"
        const val OPTION4 = "option4"
        const val TBL_QUESTIONS = "tbl_questions"
    }
}