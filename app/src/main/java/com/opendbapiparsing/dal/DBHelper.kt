package com.opendbapiparsing.dal

import android.content.Context
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

open class DBHelper(context: Context?) : SQLiteAssetHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "opendbapi.db"
        private const val DATABASE_VERSION = 1
    }
}