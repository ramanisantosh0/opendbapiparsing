package com.opendbapiparsing.model;

import java.util.List;

public class QuestionModel {
    int responseCode;
    List<QuestionDataModel> results;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public List<QuestionDataModel> getResults() {
        return results;
    }

    public void setResults(List<QuestionDataModel> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "QuestionModel{" +
                "responseCode=" + responseCode +
                ", results=" + results +
                '}';
    }
}
